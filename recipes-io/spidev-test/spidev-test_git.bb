DESCRIPTION = "SPI Loopback Tool for Testing"
SECTION = "testing"
# Taken from the Linux kernel
LICENSE = "GPL-2.0"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/GPL-2.0;md5=801f80980d171dd6425610833a22dbe6"

SRC_URI = "git://github.com/RobertBerger/spidev-test.git;protocol=https"

# Modify these as desired
PV = "1.0+git${SRCPV}"
SRCREV = "e13a97cf8c226ea273fb8c25b73b15268642bbb4"

S = "${WORKDIR}/git"

inherit cmake

# Specify any options you want to pass to cmake using EXTRA_OECMAKE:
EXTRA_OECMAKE = ""

