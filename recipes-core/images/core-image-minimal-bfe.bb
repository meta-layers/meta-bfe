require recipes-core/images/core-image-minimal.bb

# from upstream recipes
IMAGE_INSTALL += "minicom dropbear socat ntp nano ethtool htop libgpiod util-linux"

# self made
IMAGE_INSTALL += "linux-serial-test spidev-test"

